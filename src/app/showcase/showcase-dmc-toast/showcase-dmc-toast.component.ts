import { Component, OnInit } from '@angular/core';
import { DmcToastService } from 'dmc-toast';

@Component({
  selector: 'app-showcase-dmc-toast',
  templateUrl: './showcase-dmc-toast.component.html',
  styleUrls: ['./showcase-dmc-toast.component.css']
})
export class ShowcaseDmcToastComponent implements OnInit {

  constructor(private dmcToastService: DmcToastService) { }

  ngOnInit(): void {
  }


  addInfo() {
    this.dmcToastService.addInfoMessage('titulo', 'mensaje');
  }
  addError() {
    this.dmcToastService.addErrorMessage('titulo', 'mensaje');
  }
  addWarning() {
    this.dmcToastService.addWarningMessage('titulo', 'mensaje');
  }
  addSuccess() {
    this.dmcToastService.addSuccessMessage('titulo', 'mensaje');
  }

}
