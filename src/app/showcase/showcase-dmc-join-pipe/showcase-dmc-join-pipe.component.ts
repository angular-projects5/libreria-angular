import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase-dmc-join-pipe',
  templateUrl: './showcase-dmc-join-pipe.component.html',
  styleUrls: ['./showcase-dmc-join-pipe.component.css']
})
export class ShowcaseDmcJoinPipeComponent implements OnInit {

  valoresNulo = null;
  valores = [];

  constructor() {
    this.valores = [
      'valor1',
      'valor2',
      'valor3',
      'valor4',
      'valor5',
    ];
  }

  ngOnInit(): void {
  }

}
