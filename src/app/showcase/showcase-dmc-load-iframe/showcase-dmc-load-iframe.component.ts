import { Component, OnInit } from '@angular/core';
import { DmcSpinnerService } from 'dmc-spinner';

@Component({
  selector: 'app-showcase-dmc-load-iframe',
  templateUrl: './showcase-dmc-load-iframe.component.html',
  styleUrls: ['./showcase-dmc-load-iframe.component.css']
})
export class ShowcaseDmcLoadIframeComponent implements OnInit {

  constructor(private dmcSpinnerService: DmcSpinnerService) {
    this.dmcSpinnerService.showSpinner();
  }

  ngOnInit(): void {
  }

  load(evento) {
    console.log(evento);
    this.dmcSpinnerService.hideSpinner();
  }

}
