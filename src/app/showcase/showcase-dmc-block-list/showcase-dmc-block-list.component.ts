import { Component, OnInit } from '@angular/core';
import { DmcBlockItem, DmcAction } from 'dmc-block-list';

@Component({
  selector: 'app-showcase-dmc-block-list',
  templateUrl: './showcase-dmc-block-list.component.html',
  styleUrls: ['./showcase-dmc-block-list.component.css']
})
export class ShowcaseDmcBlockListComponent implements OnInit {

  public CANCEL_APOINTMENT = 'CANCEL_APOINTMENT';
  public BEFORE_APPOINTMENTS = 'BEFORE_APPOINTMENTS';
  public blockItems: DmcBlockItem[] = [];

  constructor() { }

  ngOnInit(): void {
    const items = [
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      },
    ];

    const actions: DmcAction[] = [
      {
        label: 'Cancelar cita',
        value: this.CANCEL_APOINTMENT
      },
      {
        label: 'Ver anteriores citas',
        value: this.BEFORE_APPOINTMENTS,
        icon: 'fa fa-file'
      },
    ];

    items.forEach(item => {
      const blockItem = new DmcBlockItem();
      blockItem.item = item;
      blockItem.borderColor = this.getBorderColorByStatus(item.status);
      blockItem.actions = item.age >= 29 ? actions : actions.filter((act, index) => index === 0);
      this.blockItems.push(blockItem);
    });
  }
  private getBorderColorByStatus(status: string): string {
    switch (status) {
      case 'complete':
        return 'green';
      case 'cancelled':
        return 'red';
      case 'in course':
        return 'blue';
      case 'complete':
        return 'grey';
      default:
        break;
    }
  }

  getAction($event) {
    console.log($event);
  }
  selectItem($event) {
    console.log($event);
  }

}
