import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase-dmc-click-outside',
  templateUrl: './showcase-dmc-click-outside.component.html',
  styleUrls: ['./showcase-dmc-click-outside.component.css']
})
export class ShowcaseDmcClickOutsideComponent implements OnInit {

  enabled = true;
  delay: number;

  constructor() { }

  ngOnInit(): void {
  }

  clickOutside(evento) {
    console.log(evento);

  }

  toggleEnabled() {
    this.enabled = !this.enabled;
  }
  setDelay(delay: number) {
    this.delay = delay;
  }
  removeDelay() {
    this.delay = null;
  }


}
