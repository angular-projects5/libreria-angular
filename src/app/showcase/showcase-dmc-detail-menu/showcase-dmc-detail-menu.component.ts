import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase-dmc-detail-menu',
  templateUrl: './showcase-dmc-detail-menu.component.html',
  styleUrls: ['./showcase-dmc-detail-menu.component.css']
})
export class ShowcaseDmcDetailMenuComponent implements OnInit {

  showDetail = false;

  constructor() { }

  ngOnInit(): void {
  }

  openDetail() {
    this.showDetail = true;
  }
  closeDetail() {
    this.showDetail = false;
  }

}
