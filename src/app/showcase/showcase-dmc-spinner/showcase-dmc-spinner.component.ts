import { Component, OnInit } from '@angular/core';
import { DmcSpinnerService } from 'dmc-spinner';

@Component({
  selector: 'app-showcase-dmc-spinner',
  templateUrl: './showcase-dmc-spinner.component.html',
  styleUrls: ['./showcase-dmc-spinner.component.css']
})
export class ShowcaseDmcSpinnerComponent implements OnInit {

  public embedded = false;

  constructor(private dmcSpinnerService: DmcSpinnerService) { }

  ngOnInit(): void {
  }

  activarSpinner() {
    this.dmcSpinnerService.showSpinner();

    setTimeout(() => {
      this.dmcSpinnerService.hideSpinner();
    }, 5000);
  }
  cambiarModo() {
    this.embedded = !this.embedded;
  }

}
