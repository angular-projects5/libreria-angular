import { Component, OnInit } from '@angular/core';
import { DmcSelectItem } from 'dropdown';

@Component({
  selector: 'app-showcase-dmc-dropdown',
  templateUrl: './showcase-dmc-dropdown.component.html',
  styleUrls: ['./showcase-dmc-dropdown.component.css']
})
export class ShowcaseDmcDropdownComponent implements OnInit {

  options: DmcSelectItem[] = [];
  valueSelect = 'value3';

  constructor() { }

  ngOnInit(): void {
    this.options = [
      new DmcSelectItem('label1', 'value1'),
      new DmcSelectItem('label2', 'value2'),
      new DmcSelectItem('label3', 'value3'),
      new DmcSelectItem('label4', 'value4')

    ];
  }

  selectItem(evento) {
    console.log(evento);

  }

}
