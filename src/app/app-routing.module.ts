import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowcaseDmcExampleComponent } from './showcase/showcase-dmc-example/showcase-dmc-example.component';
import { ShowcaseDmcJoinPipeComponent } from './showcase/showcase-dmc-join-pipe/showcase-dmc-join-pipe.component';
import { ShowcaseDmcSpinnerComponent } from './showcase/showcase-dmc-spinner/showcase-dmc-spinner.component';
import { ShowcaseDmcToastComponent } from './showcase/showcase-dmc-toast/showcase-dmc-toast.component';
import { ShowcaseDmcClickOutsideComponent } from './showcase/showcase-dmc-click-outside/showcase-dmc-click-outside.component';
import { ShowcaseDmcDropdownComponent } from './showcase/showcase-dmc-dropdown/showcase-dmc-dropdown.component';
import { ShowcaseDmcLoadIframeComponent } from './showcase/showcase-dmc-load-iframe/showcase-dmc-load-iframe.component';
import { ShowcaseDmcDetailMenuComponent } from './showcase/showcase-dmc-detail-menu/showcase-dmc-detail-menu.component';
import { ShowcaseDmcBlockListComponent } from './showcase/showcase-dmc-block-list/showcase-dmc-block-list.component';


const routes: Routes = [
  { path: 'dmc-example', component: ShowcaseDmcExampleComponent },
  { path: 'dmc-join-pipe', component: ShowcaseDmcJoinPipeComponent },
  { path: 'dmc-spinner', component: ShowcaseDmcSpinnerComponent },
  { path: 'dmc-toast', component: ShowcaseDmcToastComponent },
  { path: 'click-outside', component: ShowcaseDmcClickOutsideComponent },
  { path: 'dropdown', component: ShowcaseDmcDropdownComponent },
  { path: 'load-iframe', component: ShowcaseDmcLoadIframeComponent },
  { path: 'detail-menu', component: ShowcaseDmcDetailMenuComponent },
  { path: 'block-list', component: ShowcaseDmcBlockListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
