import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DmcExampleModule } from 'dmc-example';
import { ShowcaseDmcExampleComponent } from './showcase/showcase-dmc-example/showcase-dmc-example.component';
import { DmcJoinPipeModule } from 'dmc-join-pipe';
import { ShowcaseDmcJoinPipeComponent } from './showcase/showcase-dmc-join-pipe/showcase-dmc-join-pipe.component';
import { ShowcaseDmcSpinnerComponent } from './showcase/showcase-dmc-spinner/showcase-dmc-spinner.component';
import { DmcSpinnerModule } from 'dmc-spinner';
import { ShowcaseDmcToastComponent } from './showcase/showcase-dmc-toast/showcase-dmc-toast.component';
import { DmcToastModule } from 'dmc-toast';
import { ClickOutsideModule } from 'dmc-click-outside';
import { ShowcaseDmcClickOutsideComponent } from './showcase/showcase-dmc-click-outside/showcase-dmc-click-outside.component';
import { DropdownModule } from 'dmc-dropdown';
import { ShowcaseDmcDropdownComponent } from './showcase/showcase-dmc-dropdown/showcase-dmc-dropdown.component';
import { LoadIframeModule } from 'dmc-load-iframe';
import { ShowcaseDmcLoadIframeComponent } from './showcase/showcase-dmc-load-iframe/showcase-dmc-load-iframe.component';
import { DetailMenuModule } from 'dmc-detail-menu';
import { ShowcaseDmcDetailMenuComponent } from './showcase/showcase-dmc-detail-menu/showcase-dmc-detail-menu.component';
import { BlockListModule } from 'dmc-block-list';
import { ShowcaseDmcBlockListComponent } from './showcase/showcase-dmc-block-list/showcase-dmc-block-list.component';


@NgModule({
  declarations: [
    AppComponent,
    ShowcaseDmcExampleComponent,
    ShowcaseDmcJoinPipeComponent,
    ShowcaseDmcSpinnerComponent,
    ShowcaseDmcToastComponent,
    ShowcaseDmcClickOutsideComponent,
    ShowcaseDmcDropdownComponent,
    ShowcaseDmcLoadIframeComponent,
    ShowcaseDmcDetailMenuComponent,
    ShowcaseDmcBlockListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DmcExampleModule,
    DmcJoinPipeModule,
    DmcSpinnerModule,
    DmcToastModule,
    ClickOutsideModule,
    DropdownModule,
    LoadIframeModule,
    DetailMenuModule,
    BlockListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
