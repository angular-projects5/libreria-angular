import { Injectable } from '@angular/core';
import { DmcToast } from './dmc-toast-new/models/dmc-toast';
import { DmcConstants } from './dmc-toast-new/models/dmc-constants';

@Injectable({
  providedIn: 'root'
})
export class DmcToastService {

  private _toasts: DmcToast[];
  private _timeout = DmcConstants.TIMEOUT_DEFAULT;

  public get toasts(): DmcToast[] {
    return this._toasts;
  }

  public get timeout(): number {
    return this._timeout;
  }

  public set timeout(timeOut: number) {
    this._timeout = timeOut;
  }

  constructor() {
    this._toasts = [];
  }

  addInfoMessage(title: string, message: string) {
    this.addMessage(title, message, DmcConstants.STATUS_INFO);
  }
  addWarningMessage(title: string, message: string) {
    this.addMessage(title, message, DmcConstants.STATUS_WARNING);
  }
  addErrorMessage(title: string, message: string) {
    this.addMessage(title, message, DmcConstants.STATUS_ERROR);
  }
  addSuccessMessage(title: string, message: string) {
    this.addMessage(title, message, DmcConstants.STATUS_SUCCESS);
  }

  private addMessage(title: string, message: string, status: string) {
    const toast = new DmcToast(title, message, status);
    this._toasts.push(toast);
    setTimeout(() => {
      this.closeToast(toast);
    }, this._timeout);
  }

  public closeToast(toast: DmcToast) {
    const index = this._toasts.findIndex(t => t === toast);
    this.toasts.splice(index, 1);
  }
}
