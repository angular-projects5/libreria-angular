import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmcToastComponent } from './dmc-toast.component';

describe('DmcToastComponent', () => {
  let component: DmcToastComponent;
  let fixture: ComponentFixture<DmcToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmcToastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmcToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
