import { trigger, state, style, transition, animate } from '@angular/animations';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DmcToastService } from '../dmc-toast.service';

@Component({
  selector: 'lib-dmc-toast-new',
  templateUrl: './dmc-toast-new.component.html',
  styleUrls: ['./dmc-toast-new.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('overlayAnimation', [
      state('void', style({
        transform: 'translateY(5%)',
        opacity: 0
      })),
      state('visible', style({
        transform: 'translateY(0)',
        opacity: '1'
      })),
      transition('void => visible', animate('225ms ease-out')),
      transition('visible => void', animate('195ms ease-in')),
    ])
  ]
})
export class DmcToastNewComponent implements OnInit {

  @Input() timeout: number;

  constructor(public dmcToastService: DmcToastService) { }

  ngOnInit(): void {
    if (this.timeout) {
      this.dmcToastService.timeout = this.timeout;
    }
  }

}
