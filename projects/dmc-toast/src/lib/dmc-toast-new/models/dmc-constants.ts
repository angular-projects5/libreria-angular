export class DmcConstants {

    static STATUS_INFO = 'info';
    static STATUS_WARNING = 'warning';
    static STATUS_ERROR = 'danger';
    static STATUS_SUCCESS = 'success';

    static TIMEOUT_DEFAULT = 5000;
}
