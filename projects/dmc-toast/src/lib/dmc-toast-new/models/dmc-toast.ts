export class DmcToast {

    constructor(public title?: string, public message?: string, public status?: string) {

    }
}
