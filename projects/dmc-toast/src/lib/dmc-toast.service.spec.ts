import { TestBed } from '@angular/core/testing';

import { DmcToastService } from './dmc-toast.service';

describe('DmcToastService', () => {
  let service: DmcToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DmcToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
