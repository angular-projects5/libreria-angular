import { NgModule } from '@angular/core';
import { DmcToastComponent } from './dmc-toast.component';
import { DmcToastNewComponent } from './dmc-toast-new/dmc-toast-new.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [DmcToastComponent, DmcToastNewComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [DmcToastComponent, DmcToastNewComponent]
})
export class DmcToastModule { }
