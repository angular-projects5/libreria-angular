import { NgModule } from '@angular/core';
import { BlockListItemComponent } from './block-list-item/block-list-item.component';
import { BlockListComponent } from './block-list/block-list.component';
import { BlockDataItemComponent } from './block-data-item/block-data-item.component';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
  declarations: [BlockListComponent, BlockListItemComponent, BlockDataItemComponent],
  imports: [
    CommonModule,
    NgxPaginationModule
  ],
  exports: [BlockListComponent, BlockDataItemComponent]
})
export class BlockListModule { }
