export class DmcAction {

    label: string;
    value?: string;
    icon?: string;
    item?: any;
}
