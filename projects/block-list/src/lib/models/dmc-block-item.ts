import { DmcAction } from './dmc-actions';

export class DmcBlockItem {

    item: any;
    borderColor?: string;
    actions?: DmcAction[];
}
