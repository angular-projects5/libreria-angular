import { Component, OnInit, Input, EventEmitter, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { DmcBlockItem } from '../models/dmc-block-item';
import { DmcAction } from '../models/dmc-actions';

@Component({
  selector: 'dmc-block-list-item',
  templateUrl: './block-list-item.component.html',
  styleUrls: ['./block-list-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlockListItemComponent implements OnInit {

  @Input() templateHeader: TemplateRef<any>;
  @Input() templateInfoAdditional: TemplateRef<any>;
  @Input() templateData: TemplateRef<any>;

  @Input() blockItem: DmcBlockItem;
  @Input() id: string;

  @Input() showHeader: boolean;
  @Input() showInfoAdditional: boolean;
  @Input() showActions: boolean;
  @Input() showBorder: boolean;

  @Output() actionSelected = new EventEmitter<DmcAction>();
  @Output() closeOptions = new EventEmitter<string>();

  public showOptions = false;

  constructor() { }

  ngOnInit(): void {
    this.showActions = this.showActions && this.blockItem.actions && !!this.blockItem.actions.length;
  }

  openOptions($event) {
    $event.stopPropagation();
    this.showOptions = !this.showOptions;
    this.closeOptions.emit(this.id);
  }
  sendAction(action, $event) {
    $event.stopPropagation();
    this.showOptions = false;
    const actionClone = { ...action };
    actionClone.item = this.blockItem.item;
    this.actionSelected.emit(actionClone);
  }

  hideOption($event) {
    this.showOptions = false;
  }

}
