import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dmc-block-data-item',
  templateUrl: './block-data-item.component.html',
  styleUrls: ['./block-data-item.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlockDataItemComponent implements OnInit {

  @Input() label: string;
  @Input() value: string;

  constructor() { }

  ngOnInit(): void {
  }

}
