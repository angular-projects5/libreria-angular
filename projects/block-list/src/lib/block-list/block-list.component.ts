import {
  Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ContentChild,
  TemplateRef, ViewChildren, QueryList
} from '@angular/core';
import { DmcBlockItem } from '../models/dmc-block-item';
import { DmcAction } from '../models/dmc-actions';
import { BlockListItemComponent } from '../block-list-item/block-list-item.component';

@Component({
  selector: 'dmc-block-list',
  templateUrl: './block-list.component.html',
  styleUrls: ['./block-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BlockListComponent implements OnInit {
  @ContentChild('templateheader', { static: false }) templateHeader: TemplateRef<any>;
  @ContentChild('templateInfoAdditional', { static: false }) templateInfoAdditional: TemplateRef<any>;
  @ContentChild('templateData', { static: false }) templateData: TemplateRef<any>;

  /**
   * Items a mostrar yeaahh
   */
  @Input() blockItems: DmcBlockItem[];
  /**
   * Texto que se mostará si no hay resultados
   */
  @Input() labelNoResults = 'No hay resultados';

  @Input() showHeader = true;
  @Input() showInfoAdditional = true;
  @Input() showActions = true;
  @Input() showBorder = true;

  @Input() pagination = 10;
  @Input() previousLabel = 'Previous';
  @Input() nextLabel = 'Next';

  @Output() ItemSelected = new EventEmitter<any>();
  @Output() actionSelected = new EventEmitter<DmcAction>();

  @ViewChildren(BlockListItemComponent) blocks: QueryList<BlockListItemComponent>;

  public page: number;

  constructor() {
    this.page = 1;
  }

  ngOnInit(): void {
    if (this.pagination < 0) {
      this.pagination = 10;
    }
  }

  closeAllOptionsExcept(id: string) {
    this.blocks.forEach(block => {
      if (block.id !== id) {
        block.showOptions = false;
      }
    });
  }

  sendAction($event) {
    this.actionSelected.emit($event);
  }
  /**
   * Emite el item seleccionado
   * @param blockItem Es el item seleccionado
   */
  selectItem(blockItem: DmcBlockItem) {
    this.ItemSelected.emit(blockItem.item);
  }

}
