# DmcBlockList

Component to show data in blocks like table.

## Installation

Using npm:

`npm install dmc-block-list`

## Usage

In app.module:

```ts
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

// Modulos
import { AppRoutingModule } from './app-routing.module';
import { BlockListModule } from 'dmc-block-list';


// Componentes
import { AppComponent } from './app.component';
import { ShowcaseDmcBlockListComponent } from './showcase/showcase-dmc-block-list/showcase-dmc-block-list.component';


@NgModule({
  declarations: [
    AppComponent,
    ShowcaseDmcBlockListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BlockListModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


```
In your ts:

```ts

import { Component, OnInit } from '@angular/core';
import { DmcBlockItem, DmcAction } from 'block-list';

@Component({
  selector: 'app-showcase-dmc-block-list',
  templateUrl: './showcase-dmc-block-list.component.html',
  styleUrls: ['./showcase-dmc-block-list.component.css']
})
export class ShowcaseDmcBlockListComponent implements OnInit {

  public CANCEL_APOINTMENT = 'CANCEL_APOINTMENT';
  public BEFORE_APPOINTMENTS = 'BEFORE_APPOINTMENTS';
  public blockItems: DmcBlockItem[] = [];

  constructor() { }

  ngOnInit(): void {
    const items = [
      {
        date: '2019-09-09',
        status: 'complete',
        client: 'David',
        age: 35
      },
      {
        date: '2019-10-10',
        status: 'cancelled',
        client: 'Juan',
        age: 18
      },
      {
        date: '2019-11-11',
        status: 'in course',
        client: 'Felipe',
        age: 42
      },
      {
        date: '2019-12-12',
        status: 'unknown',
        client: 'Pedro',
        age: 29
      },
      {
        date: '2019-13-13',
        status: 'cancelled',
        client: 'Perico',
        age: 77
      }
    ];

    const actions: DmcAction[] = [
      {
        label: 'Cancelar cita',
        value: this.CANCEL_APOINTMENT
      },
      {
        label: 'Ver anteriores citas',
        value: this.BEFORE_APPOINTMENTS,
        icon: 'fa fa-file'
      },
    ];

    items.forEach(item => {
      const blockItem = new DmcBlockItem();
      blockItem.item = item;
      blockItem.borderColor = this.getBorderColorByStatus(item.status);
      blockItem.actions = item.age >= 29 ? actions : actions.filter((act, index) => index === 0);
      this.blockItems.push(blockItem);
    });
  }
  private getBorderColorByStatus(status: string): string {
    switch (status) {
      case 'complete':
        return 'green';
      case 'cancelled':
        return 'red';
      case 'in course':
        return 'blue';
      case 'complete':
        return 'grey';
      default:
        break;
    }
  }

  getAction($event) {
    console.log($event);
  }
  selectItem($event) {
    console.log($event);
  }

}



```

In your html:

```html
<dmc-block-list [blockItems]="blockItems"
    [labelNoResults]="'No hay datos que mostrar'"
    [previousLabel]="'previo'"
    [nextLabel]="'Siguiente'"
    [pagination]="5"
    [showHeader]="true"
    [showActions]="true"
    [showInfoAdditional]="true"
    (actionSelected)="getAction($event)"
    (ItemSelected)="selectItem($event)">

    <ng-template #templateInfoAdditional
        let-item="item">
        <span>{{item.date | date: 'dd/MMM/yyyy'}}</span>
    </ng-template>
    <ng-template #templateHeader
        let-item="item">
        <span>{{item.client}}</span>
    </ng-template>
    <ng-template #templateData
        let-item="item">
        <dmc-block-data-item label="Estado"
            [value]="item.status"></dmc-block-data-item>
        <dmc-block-data-item label="Edad"
            [value]="item.age"></dmc-block-data-item>
    </ng-template>

</dmc-block-list>

```

## Inputs

| Input  | Description |
| ------------- | ------------- |
| blockItems  | Elements to show |
| labelNoResults  | Label when there are not items |
| showHeader  | No show header |
| showInfoAdditional  | No show info-additional |
| showActions  | No show actions |
| pagination  | Pagination |
| previousLabel  | Label previous pagination |
| nextLabel  | Label next pagination |

## Outputs

| Output  | Description |
| ------------- | ------------- |
| itemSelected  | Event when click in item |
| actionSelected  | Event when click action |
