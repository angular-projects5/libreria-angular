# dmcJoinPipe

Pipe to show in a string from an array.

## Installation

Using npm:

`npm install dmc-join-pipe`

## Usage

In module:

```ts

// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modules
import { AppRoutingModule } from './app-routing.module';
import { dmcJoinPipeModule } from 'dmc-join-pipe';

@NgModule({
  declarations: [
    AppComponent,
    SampledmcJoinPipeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    dmcJoinPipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```
In your ts:

```ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sample-dmc-join-pipe',
  templateUrl: './sample-dmc-join-pipe.component.html',
  styleUrls: ['./sample-dmc-join-pipe.component.css']
})
export class SampledmcJoinPipeComponent {

  valores: string[];

  constructor() {
    this.valores = [
      "valor1",
      "valor2",
      "valor3",
      "valor4",
      "valor5"
    ]

  }

}

```

In your html:

```html
{{valores | join}}
```


