import { NgModule } from '@angular/core';
import { DmcJoinPipeComponent } from './dmc-join-pipe.component';
import { DmcJoinPipe } from './dmc-join.pipe';



@NgModule({
  declarations: [DmcJoinPipeComponent, DmcJoinPipe],
  imports: [
  ],
  exports: [DmcJoinPipeComponent, DmcJoinPipe]
})
export class DmcJoinPipeModule { }
