import { TestBed } from '@angular/core/testing';

import { DmcJoinPipeService } from './dmc-join-pipe.service';

describe('DmcJoinPipeService', () => {
  let service: DmcJoinPipeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DmcJoinPipeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
