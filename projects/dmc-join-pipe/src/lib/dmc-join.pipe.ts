import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dmcJoin'
})
export class DmcJoinPipe implements PipeTransform {

  transform(value: string[], separator: string = ','): unknown {
    if (!value) { return ''; }

    return value.join(separator);
  }

}
