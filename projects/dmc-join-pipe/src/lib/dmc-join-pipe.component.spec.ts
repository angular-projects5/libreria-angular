import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmcJoinPipeComponent } from './dmc-join-pipe.component';

describe('DmcJoinPipeComponent', () => {
  let component: DmcJoinPipeComponent;
  let fixture: ComponentFixture<DmcJoinPipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmcJoinPipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmcJoinPipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
