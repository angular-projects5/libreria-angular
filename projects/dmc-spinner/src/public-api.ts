/*
 * Public API Surface of dmc-spinner
 */

export * from './lib/dmc-spinner.service';
export * from './lib/dmc-spinner.component';
export * from './lib/dmc-spinner.module';
export * from './lib/dmc-spinner-new/dmc-spinner-new.component';
