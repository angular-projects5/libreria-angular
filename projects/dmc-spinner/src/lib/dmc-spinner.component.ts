import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dmc-dmc-spinner',
  template: `
    <p>
      dmc-spinner works!
    </p>
  `,
  styles: []
})
export class DmcSpinnerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
