import { TestBed } from '@angular/core/testing';

import { DmcSpinnerService } from './dmc-spinner.service';

describe('DmcSpinnerService', () => {
  let service: DmcSpinnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DmcSpinnerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
