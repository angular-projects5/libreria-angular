import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { DmcSpinnerService } from '../dmc-spinner.service';

@Component({
  selector: 'dmc-dmc-spinner-new',
  templateUrl: './dmc-spinner-new.component.html',
  styleUrls: ['./dmc-spinner-new.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DmcSpinnerNewComponent implements OnInit {

  @Input() embedded: boolean;

  constructor(public spinnerService: DmcSpinnerService) { }

  ngOnInit(): void {
  }

}
