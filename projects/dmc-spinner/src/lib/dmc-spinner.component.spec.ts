import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmcSpinnerComponent } from './dmc-spinner.component';

describe('DmcSpinnerComponent', () => {
  let component: DmcSpinnerComponent;
  let fixture: ComponentFixture<DmcSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmcSpinnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmcSpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
