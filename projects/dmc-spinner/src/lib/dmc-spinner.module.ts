import { NgModule } from '@angular/core';
import { DmcSpinnerComponent } from './dmc-spinner.component';
import { DmcSpinnerNewComponent } from './dmc-spinner-new/dmc-spinner-new.component';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [DmcSpinnerComponent, DmcSpinnerNewComponent],
  imports: [
    CommonModule
  ],
  exports: [DmcSpinnerComponent, DmcSpinnerNewComponent]
})
export class DmcSpinnerModule { }
