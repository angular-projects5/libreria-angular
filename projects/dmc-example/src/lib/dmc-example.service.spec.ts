import { TestBed } from '@angular/core/testing';

import { DmcExampleService } from './dmc-example.service';

describe('DmcExampleService', () => {
  let service: DmcExampleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DmcExampleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
