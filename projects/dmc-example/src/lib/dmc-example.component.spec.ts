import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DmcExampleComponent } from './dmc-example.component';

describe('DmcExampleComponent', () => {
  let component: DmcExampleComponent;
  let fixture: ComponentFixture<DmcExampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DmcExampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DmcExampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
