import { NgModule } from '@angular/core';
import { DmcExampleComponent } from './dmc-example.component';



@NgModule({
  declarations: [DmcExampleComponent],
  imports: [
  ],
  exports: [DmcExampleComponent]
})
export class DmcExampleModule { }
