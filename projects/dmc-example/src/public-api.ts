/*
 * Public API Surface of dmc-example
 */

export * from './lib/dmc-example.service';
export * from './lib/dmc-example.component';
export * from './lib/dmc-example.module';
