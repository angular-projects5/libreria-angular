import { Directive, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[dmcLoadIframe]'
})
export class LoadIframeDirective {

  @Output() loadIframe: EventEmitter<boolean>;

  constructor(private el: ElementRef) {
    this.loadIframe = new EventEmitter<boolean>();
  }

  @HostListener('load')
  public onload() {
    if (!this.el.nativeElement.contentDocument
      || this.el.nativeElement.contentDocument.body.children.length > 0) {
      this.loadIframe.emit(true);
    }
  }

}
