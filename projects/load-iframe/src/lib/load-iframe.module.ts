import { NgModule } from '@angular/core';
import { LoadIframeDirective } from './load-iframe.directive';


@NgModule({
  declarations: [LoadIframeDirective],
  imports: [
  ],
  exports: [LoadIframeDirective]
})
export class LoadIframeModule { }
