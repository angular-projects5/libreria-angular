import { NgModule } from '@angular/core';
import { DetailMenuComponent } from './detail-menu/detail-menu.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [DetailMenuComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule
  ],
  exports: [DetailMenuComponent]
})
export class DetailMenuModule { }
