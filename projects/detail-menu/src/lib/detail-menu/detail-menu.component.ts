import { Component, OnInit, EventEmitter, Output, ViewEncapsulation, Input } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'dmc-detail-menu',
  templateUrl: './detail-menu.component.html',
  styleUrls: ['./detail-menu.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('slide', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('600ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('600ms ease-in', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]
})
export class DetailMenuComponent implements OnInit {

  @Output() closeModal = new EventEmitter<boolean>();

  @Input() showDetail = false;

  constructor() {
  }

  ngOnInit(): void {
  }

  closeDetail() {
    this.showDetail = false;
    setTimeout(() => {
      this.closeModal.emit(true);
    }, 600);
  }

}
