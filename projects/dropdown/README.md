# DmcDropdown

Component to show options to choose, we can filter options.

## Installation

Using npm:

`npm install dmc-dropdown`

## Usage

In app.module:

```ts


// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Modulos
import { AppRoutingModule } from './app-routing.module';
import { DropdownModule } from 'dmc-dropdown';

// Componentes
import { AppComponent } from './app.component';
import { ShowcaseDmcDropdownComponent } from './showcase/showcase-dmc-dropdown/showcase-dmc-dropdown.component';

@NgModule({
  declarations: [
    AppComponent,
    ShowcaseDmcDropdownComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DropdownModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

```
In your ts:

```ts
import { DmcSelectItem } from 'dropdown';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-showcase-dmc-dropdown',
  templateUrl: './showcase-dmc-dropdown.component.html',
  styleUrls: ['./showcase-dmc-dropdown.component.css']
})
export class ShowcaseDmcDropdownComponent implements OnInit {

  options: DmcSelectItem[];
  valueSelect: string;

  constructor() { }

  ngOnInit() {
    this.options = [
      new DmcSelectItem('Label1', 'Value1'),
      new DmcSelectItem('Label2', 'Value2'),
      new DmcSelectItem('Label3', 'Value3'),
      new DmcSelectItem('Label4', 'Value4')
    ]
    this.valueSelect = 'Value3';
  }

  selectItem($event){
    console.log($event);
  }

}

```

In your html:

```html
<div class="row">
    <div class="col-2 mt-4">

        <dmc-dropdown 
            [options]="options"
            [valueSelect]="valueSelect"
            [labelNoResults]="'No hay resultados que mostrar filtrando'"
            (selectValue)="selectItem($event)">
            <ng-template let-item="item">
                {{item.label + ' - ' + item.value}}
            </ng-template>

        </dmc-dropdown>
        <p>Este texto no se va a ver cuando se abra el dropdown</p>
    </div>
</div>

```

## Inputs

| Input  | Description |
| ------------- | ------------- |
| options  | Options to show |
| valueSelect  | Value of item to preload  |
| labelNoResults  | Label to show when filter items and not found |

## Outputs

| Output  | Description |
| ------------- | ------------- |
| selectValue  | Event when select item |
