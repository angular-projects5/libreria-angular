import { Component, OnInit, Input, Output, EventEmitter, ViewEncapsulation, ContentChild, TemplateRef } from '@angular/core';
import { DmcSelectItem } from '../models/dmc-select-item';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'dmc-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('overlayAnimation', [
      state('void', style({
        transform: 'translateY(5%)',
        opacity: 0
      })),
      state('visible', style({
        transform: 'translateY(0)',
        opacity: '1'
      })),
      transition('void => visible', animate('225ms ease-out')),
      transition('visible => void', animate('195ms ease-in')),
    ])
  ]
})
export class DropdownComponent implements OnInit {
  // coge lo de dentro de un componente
  @ContentChild(TemplateRef, { static: false }) templateCogerContent: TemplateRef<any>;

  @Input() options: DmcSelectItem[] = [];
  @Input() valueSelect: any;
  @Input() labelNoResults = 'No hay resultados';

  @Output() selectValue = new EventEmitter<DmcSelectItem>();

  public showItems = false;
  public optionsShow: DmcSelectItem[];
  public valueShow: string;

  constructor() { }

  ngOnInit(): void {
    if (this.valueSelect) {
      this.preload();
    }
    this.optionsShow = [...this.options];
  }

  preload() {
    const optionFound = this.options.find(option => option.value === this.valueSelect);

    if (optionFound) {
      this.valueShow = optionFound.label;
      this.selectItem(optionFound);
    }
  }

  showPanelOptions() {
    this.showItems = !this.showItems;
  }

  filter(searchWord: string) {
    this.optionsShow = this.options.filter(op => op.label.toLocaleLowerCase().indexOf(searchWord.toLocaleLowerCase()) > -1);
  }

  selectItem(item) {
    this.showItems = false;
    this.valueShow = item.label;
    this.selectValue.emit(item);
  }

  pintarItem(item) {
    console.log(item);

  }

  hidePanelItems(evento) {
    this.showItems = false;
  }

}
