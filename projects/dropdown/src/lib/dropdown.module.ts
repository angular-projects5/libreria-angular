import { NgModule } from '@angular/core';
import { DropdownComponent } from './dropdown/dropdown.component';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClickOutsideModule } from 'dmc-click-outside';



@NgModule({
  declarations: [DropdownComponent],
  imports: [
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    ClickOutsideModule
  ],
  exports: [DropdownComponent]
})
export class DropdownModule { }
