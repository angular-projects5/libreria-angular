export class DmcSelectItem {

    label: string;
    value: any;

    constructor(label?: string, value?: any) {
        this.label = label;
        this.value = value;
    }
}
