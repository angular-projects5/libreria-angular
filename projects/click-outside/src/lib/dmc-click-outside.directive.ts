import { Directive, Input, Output, EventEmitter, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[dmcClickOutside]'
})
export class DmcClickOutsideDirective {

  @Input() clickOutsideEnabled = true;
  @Input() clickOutsideDelay: number;

  @Output() clickOutside = new EventEmitter<MouseEvent>();

  constructor(private elementRef: ElementRef) { }

  @HostListener('document:click', ['$event'])
  public onDocumentClick(evento: MouseEvent) {
    if (this.clickOutsideEnabled) {
      const target = evento.target as HTMLElement;

      if (target && !this.elementRef.nativeElement.contains(target)) {

        if (this.clickOutsideDelay) {
          setTimeout(() => {
            this.clickOutside.emit(evento);
          }, this.clickOutsideDelay);
          return;
        }

        this.clickOutside.emit(evento);
      }

    }
  }

}
