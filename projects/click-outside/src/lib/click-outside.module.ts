import { NgModule } from '@angular/core';
import { DmcClickOutsideDirective } from './dmc-click-outside.directive';


@NgModule({
  declarations: [DmcClickOutsideDirective],
  imports: [
  ],
  exports: [DmcClickOutsideDirective]
})
export class ClickOutsideModule { }
